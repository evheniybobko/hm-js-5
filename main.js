function createNewUser (userFirstName, userLastName) {
    userFirstName = prompt("Введіть ваше ім'я");
    userLastName = prompt("Введіть ваше прізвище");

    return {
        _firstName: userFirstName,
        _lastName: userLastName,

        get login() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },

        get firstName() {
            return this._firstName;
        },

        set firstName(value) {
            if (isNaN(Number(value))) {
                return this._firstName = value;
            }
        },

        get lastName() {
            return this._lastName;
        },

        set lastName(value) {
            if (isNaN(Number(value))) {
                return this._lastName = value;
            }
        }
    };
}

const newUser = createNewUser();

console.log(newUser.login);




/* 1. Опишіть своїми словами, що таке метод об'єкту

Метод об'єкту в - це функція, яка є частиною об'єкта і може викликатися для виконання певних дій або операцій з даними об'єкта.
У JavaScript об'єкти - це особливы типи даних, які дозволяють зберігати і організовувати дані у вигляді пар ключ-значення.

*/



/* 2. Який тип даних може мати значення властивості об'єкта?

Це може бути будь-який тип даних, якій є в JavaScript. Це можуть бути числа, рядки, булеві значення, null, undefined.

*/



/* 3.Об'єкт це посилальний тип даних. Що означає це поняття?

Це означає, що змінна, яка містить об'єкт, насправді містить не сам об'єкт, а посилання (або адресу пам'яті), де зберігається цей об'єкт, 
при копіюванні або передачі об'єкта в іншу змінну, передається саме посилання, а не новий об'єкт.

*/